function PriceChange(price, change_date){
	this.price = price || 0
	this.change_date = change_date || new Date()
}

PriceChange.prototype.price_diff = function(other){
	var diff = (this.price - other.price) * 100 / this.price
	return diff
}

PriceChange.prototype.time_diff = function(other){
	return PriceChange.date_diff_in_days(this.change_date, other.change_date)
}

PriceChange.date_diff_in_days = function(start, end){
	return (start - end) / (1000 * 3600 * 24)
}
