function Item(price, date_price_changed) {
	this.price_change_history = price ? 
		[new PriceChange(price, date_price_changed)] : []
	this.promotion_started = null
}

Item.prototype.set_price = function(price, change_date){
	change_date = change_date || new Date()

	this.price_change_history.push(new PriceChange(price, change_date))

	if(this.should_stop_promotion())
		this.abort_promotion()

	if(this.can_start_promotion())
		this.start_promotion()
}

Item.prototype.get_price = function(){
	return this.get_price_change().price
}

Item.prototype.get_price_change = function(history_setp){
	var step = history_setp || 1
	return this.price_change_history[this.price_change_history.length - step]
}

Item.prototype.abort_promotion = function(){
	this.promotion_started = null
}

Item.prototype.start_promotion = function(start_date){
	this.promotion_started = this.get_price_change()
}

Item.prototype.should_stop_promotion = function(){
	if(null == this.promotion_started)
		return false

	var diff = this.promotion_started.price_diff(this.get_price_change())
		
	return diff < 0 || diff >= 30
}

Item.prototype.can_start_promotion = function(){
	var previous_price = this.get_price_change(2)

	if(!previous_price)
		return false

	var price = this.get_price_change()

	var diff = previous_price.price_diff(price)
	var last_price_valid_days = price.time_diff(previous_price)
	
	return diff >= 5 && 
		diff < 30 && 
		last_price_valid_days >= 30 
}

Item.prototype.has_promotion = function(date_at){
	date_at = date_at || new Date()
	return this.promotion_started != null && 
		Item.date_diff_in_days(date_at, this.promotion_started.change_date) < 30
}

Item.date_diff_in_days = function(start, end){
	return (start - end) / (1000 * 3600 * 24)
}
