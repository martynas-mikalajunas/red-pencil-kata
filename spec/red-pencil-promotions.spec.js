describe('promotions', function(){

  function given_item_with_price_before_days(price, days){
    return  new Item(100, date_before_days(days))
  }

  function date_after_days(days){
    return date_before_days(-days)
  }

  function date_before_days(days){
    var before_days = new Date()
    before_days.setDate((new Date()).getDate() - days)
    return before_days
  }

	var item;

	describe('last price changed before 30 days', function(){

		beforeEach(function(){
			item = given_item_with_price_before_days(100, 30)
		})

		it('starts after price reduced more than 5%', function(){
			item.set_price(95)
			expect(item.has_promotion()).toBe(true)
		})

		it('does not start if price reduced less than 5%', function(){
			item.set_price(96)
			expect(item.has_promotion()).toBe(false)
		})

		it('does not start if price reduced more than 30%', function(){
			item.set_price(69)
			expect(item.has_promotion()).toBe(false)
		})

	})

	describe('last price changed before 29 days', function(){

		beforeEach(function(){
			item = given_item_with_price_before_days(100, 29)
		})

		it('does not start if old price was stable less than 30 days', function(){
			item.set_price(95)
			expect(item.has_promotion()).toBe(false)
		})
	})

	describe('last price changed before 61 days', function(){

		beforeEach(function(){
			item = given_item_with_price_before_days(100, 69)
		})

		it('stops after 30 days', function(){
			item.set_price(95, date_before_days(30))
			expect(item.has_promotion()).toBe(false)
		})
	})

	describe('price change during promotion', function(){

		beforeEach(function(){
			item = given_item_with_price_before_days(100, 51)
			item.set_price(95, date_before_days(21))
		})

    it('price change during promotion does not prolong it', function(){
      item.set_price(90)
      expect(item.has_promotion(date_after_days(20))).toBe(false)
    })

		it('stops if price goes up', function(){
			expect(item.has_promotion()).toBe(true)
			item.set_price(96)
			expect(item.has_promotion()).toBe(false)
		})

		it('stops if overall reduction during promotion more than 30%', function(){
      item.set_price(90)
      expect(item.has_promotion()).toBe(true)
      item.set_price(65)
      expect(item.has_promotion()).toBe(false)
		})

	})

	describe('new promotion', function(){
		it('can start again if conditions are met', function(){
			var item = given_item_with_price_before_days(100, 65)
			item.set_price(95, date_before_days(35))
			expect(item.has_promotion(date_before_days(35))).toBe(true, 'first prce reduction gives promotion')
			item.set_price(96, date_before_days(34))
			expect(item.has_promotion(date_before_days(34))).toBe(false, 'price up, promotion ends')
			item.set_price(90)
			expect(item.has_promotion()).toBe(true, 'new promotion should start')
		})
	})
})
